/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/01 11:16:09 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>

#pragma weak main

uint32_t	adder(uint32_t a, uint32_t b)
{
	uint32_t carry;

	while (b != 0)
	{
		carry = a & b;
		a = a ^ b;
		b = carry << 1;
	}
	return (a);
}

uint32_t multiplier(uint32_t a, uint32_t b)
{
	uint32_t	sum;

	sum = 0;
	for (uint32_t i = 0; i < b; ++i)
		sum = adder(sum, a);
	return (sum);
}

void	test_multiplier(uint32_t a, uint32_t b)
{
	printf("%u * %u = %u = %u\n", a, b, a * b, multiplier(a, b));
}

int main(void)
{
	test_multiplier(0, 0);
	for (int i = 0; i < 255; ++i)
		test_multiplier(i, 1);
	for (int i = 0; i < 255; ++i)
		test_multiplier(255, i);
}
