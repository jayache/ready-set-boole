/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:02:45 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "tree.h"

#pragma weak main

bool	eval_tree(t_node *node)
{
	char	val;
	t_node	*left;
	t_node	*right;

	left = node->left;
	right = node->right;
	val = node->value;
	if ((!left || !right) && val != '1' && val != '0' && val != '!')
		printf("WARNING: Not all children set for %c\n", val);
	if (val == '1')
		return (true);
	else if (val == '0')
		return false;
	else if (val == '!')
		return !eval_tree(node->left);
	else if (val == '&')
		return eval_tree(node->left) && eval_tree(node->right);
	else if (val == '|')
		return eval_tree(node->left) || eval_tree(node->right);
	else if (val == '>')
		return !(eval_tree(node->left) && !eval_tree(node->right));
	else if (val == '=')
		return (eval_tree(node->left) == eval_tree(node->right));
	else if (val == '^')
		return (eval_tree(node->left) ^ eval_tree(node->right));
	return (false);
}

bool	eval_formula(char	*formula)
{
	t_node	*tree;
	bool	ret;

	tree = generate_ast(formula);
	ret = eval_tree(tree);	
	free_tree(tree);
	return (ret);
}

void	test_eval(char *formula)
{
	printf("%s -> %d\n", formula, eval_formula(formula));
}

int main(int ac, char **av)
{
	t_node *tree;

	if (ac != 1)
	{
		test_eval(av[1]);
		tree = generate_ast(av[1]);
		print_tree(tree, 0);
		free_tree(tree);
	}
	else
	{
		test_eval("10&");
		test_eval("1!");
		test_eval("10^");
		test_eval("10|");
		test_eval("10=");
		test_eval("10>");
		test_eval("11>");
		test_eval("10|1&");
		test_eval("101|&");
		test_eval("1011||=");
	}
}
