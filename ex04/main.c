/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:02:51 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

#pragma weak main

bool	eval_tree(t_node *node, uint32_t truth)
{
	char	val;
	t_node	*left;
	t_node	*right;

	left = node->left;
	right = node->right;
	val = node->value;
	if (val == '1')
		return (true);
	else if (val == '0')
		return false;
	else if (val >= 'A' && val <= 'Z')
	{
		return ((truth >> (val - 'A')) & 1);
	}
	else if (val == '!')
		return !eval_tree(left, truth);
	else if (val == '&')
		return eval_tree(left, truth) && eval_tree(right, truth);
	else if (val == '|')
		return eval_tree(left, truth) || eval_tree(right, truth);
	else if (val == '>')
		return !(eval_tree(left, truth) && !eval_tree(right, truth));
	else if (val == '=')
		return (eval_tree(left, truth) == eval_tree(right, truth));
	else if (val == '^')
		return (eval_tree(left, truth) ^ eval_tree(right, truth));
	return (false);
}


void	print_truth_table(char	*formula)
{
	t_node	*tree;
	int		letters;
	int		var_number;

	var_number = 0;
	letters = 0;
	tree = generate_ast(formula);
	for (int i = 0; i < 26; ++i)
	{
		if (strchr(formula, 'A' + i))
		{
			letters |= (1 << i);
			var_number += 1;
			printf("| %c ", 'A' + i);
		}
	}
	printf("| = |\n");
	for (int i = 0; i < var_number; ++i)
		printf("|---");
	printf("|---|\n");
	for (uint32_t i = 0; i < (67108864); ++i)
	{
		if (i & ~letters)
			continue;
		for (int x = 0; x < 26; ++x)
		{
			if (strchr(formula, 'A' + x))
			{
				printf("| %d ", (i >> x) & 1);
			}
		}
		bool b = eval_tree(tree, i);
		printf("| %d |\n", b);
	}
	free_tree(tree);
}

void	test_truth_table(char *formula)
{
	printf("Truth table of %s\n", formula);
	print_truth_table(formula);
}

int main(int ac, char **av)
{
	if (ac != 1)
	{
		test_truth_table(av[1]);
		print_tree(generate_ast(av[1]), 0);
	}
	else
	{
		test_truth_table("AB&");
		test_truth_table("AB|");
		test_truth_table("AB=");
		test_truth_table("AB^");
		test_truth_table("A1^");
		test_truth_table("A!");
		test_truth_table("AB&C|");
		test_truth_table("AQ&Z|");
	}
}
