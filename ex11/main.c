/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 11:07:13 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 11:03:25 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>

#pragma weak main

double map(uint16_t x, uint16_t y)
{
	double ret;

	ret = (((uint32_t)x << 16) + y) / (double)0xffffffff;
	return (ret);
}

uint16_t	*reverse_map(double n)
{
	static uint16_t ret[2];

	n *= 0xffffffff;
	ret[1] = (uint32_t)n / 0xffff;
	ret[0] = (uint32_t)n % 0xffff;
	ret[0] -= ret[1];
	if (n/ 0xffffffff > 0.9)
		ret[1]--;
	return (ret);
}

void	test_reverse_map(uint16_t x, uint16_t y)
{
	uint16_t	*ret;
	double		n;

	n = map(x, y);
	ret = reverse_map(n);
	printf("(%u, %u) -> %.15lf -> (%u, %u)\n", x, y, n, ret[1], ret[0]);
}

int main()
{
	for (int i = 1; i < 0xffff; i *= 2)
	{
		test_reverse_map(i - 1, 5);
	}
	for (int i = 1; i < 0xffff; i *= 2)
	{
		test_reverse_map(0xffff - 5, i - 1);
	}
}
