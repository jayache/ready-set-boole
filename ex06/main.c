/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:03:01 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

#pragma weak main

char	*conjunctive_normal_form(char *formula)
{
	t_node	*tree;
	char	*form;

	tree = generate_ast(formula);
	generate_cnf(tree);
	form = formula_from_ast(tree);
	free_tree(tree);
	return (form);
}

void	test_cnf(char *formula)
{
	char	*form;
	
	form = conjunctive_normal_form(formula);
	printf("%s -> %s\n", formula, form);
	free(form);
}

int main(int ac, char **av)
{
	if (ac != 1)
	{
		t_node *tree = generate_ast(av[1]);
		generate_cnf(tree);
		test_cnf(av[1]);
		print_tree(tree, 0);
		free_tree(tree);
	}
	else
	{
		test_cnf("AB&!");
		test_cnf("AB|!");
		test_cnf("AB|C&");
		test_cnf("AB|C|D|");
		test_cnf("AB&C&D&");
		test_cnf("AB&!C!|");
		test_cnf("AB|!C!&");
	}
}
