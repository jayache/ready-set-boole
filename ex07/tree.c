/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 13:10:16 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 10:44:40 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "tree.h"

t_node	*node(char value)
{
	t_node *ret;

	ret = (t_node*)malloc(sizeof(t_node));
	ret->right = NULL;
	ret->left = NULL;
	ret->value = value;
	return (ret);
}

t_node	*node_copy(t_node *node)
{
	t_node *ret;

	if (!node)
		return (NULL);
	ret = (t_node*)malloc(sizeof(t_node));
	ret->right = node_copy(node->right);
	ret->left = node_copy(node->left);
	ret->value = node->value;
	return (ret);
}

void	print_tree(t_node *node, int level)
{
	if (!node)
		return;
	if (level == 0)
		printf("%c\n", node->value);
	else
	{
		printf("%*s%c -- %d\n", level + 3, "╠", node->value, level);
	}
	print_tree(node->left, level + 1);
	print_tree(node->right, level + 1);
}

int		tree_size(t_node *tree)
{
	if (!tree)
		return (0);
	return (1 + tree_size(tree->right) + tree_size(tree->left));
}

static int		insert_in_formula(t_node *tree, char *formula, int index)
{
	if (!tree)
		return (index);
	index = insert_in_formula(tree->right, formula, index);
	index = insert_in_formula(tree->left, formula, index);
	formula[index++] = tree->value;
	return (index);
}


char	*formula_from_ast(t_node *tree)
{
	int	size;
	char	*formula;

	size = tree_size(tree);
	formula = malloc(sizeof(char*) * (size+1));
	formula[size] = '\0';
	insert_in_formula(tree, formula, 0);
	return (formula);
}

t_node	*generate_ast(char *formula)
{
	t_node *stack[100] = {0};
	int	sp = -1;

	for (int i = 0; formula[i]; ++i)
	{
		char c = formula[i];
		if (c == '1' || c == '0')
		{
			stack[++sp] = node(c);
		}
		else if (c >= 'A' && c <= 'Z')
		{
			stack[++sp] = node(c);
		}
		else if (c == '!')
		{
			t_node *a = stack[sp--];
			t_node *elem = node(c);
			elem->left = a;
			stack[++sp] = elem;
		}
		else
		{
			t_node *a = stack[sp--];
			t_node *b = stack[sp--];
			t_node *elem = node(c);
			elem->left = b;
			elem->right = a;
			stack[++sp] = elem;
		}
	}
	return (stack[0]);
}

void	free_tree(t_node *tree)
{
	if (!tree)
		return;
	free_tree(tree->left);
	free_tree(tree->right);
	free(tree);
}
