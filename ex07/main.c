/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:03:07 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

#pragma weak main

bool	eval_tree(t_node *node, uint32_t truth)
{
	char	val;
	t_node	*left;
	t_node	*right;

	left = node->left;
	right = node->right;
	val = node->value;
	if (val == '1')
		return (true);
	else if (val == '0')
		return false;
	else if (val >= 'A' && val <= 'Z')
	{
		return ((truth >> (val - 'A')) & 1);
	}
	else if (val == '!')
		return !eval_tree(left, truth);
	else if (val == '&')
		return eval_tree(left, truth) && eval_tree(right, truth);
	else if (val == '|')
		return eval_tree(left, truth) || eval_tree(right, truth);
	else if (val == '>')
		return !(eval_tree(left, truth) && !eval_tree(right, truth));
	else if (val == '=')
		return (eval_tree(left, truth) == eval_tree(right, truth));
	else if (val == '^')
		return (eval_tree(left, truth) ^ eval_tree(right, truth));
	return (false);
}

bool	eval_truth_table(char	*formula)
{
	t_node	*tree;
	int		letters;
	int		var_number;
	bool	ret;

	ret = false;
	var_number = 0;
	letters = 0;
	tree = generate_ast(formula);
	for (int i = 0; i < 26; ++i)
	{
		if (strchr(formula, 'A' + i))
		{
			letters |= (1 << i);
			var_number += 1;
		}
	}
	for (uint32_t i = 0; i < (67108864); ++i)
	{
		if (i & ~letters)
			continue;
		ret |= eval_tree(tree, i);
		if (ret)
			break;
	}
	free_tree(tree);
	return (ret);
}

bool	sat(char *formula)
{
	return (eval_truth_table(formula));
}

void	test_sat(char *formula)
{
	printf("%s -> %d\n", formula, sat(formula));
}

int main(int ac, char **av)
{
	if (ac != 1)
	{
		test_sat(av[1]);
	}
	else
	{
		test_sat("AB&!");
		test_sat("AB|!");
		test_sat("AB|C&");
		test_sat("AB|C|D|");
		test_sat("AB&C&D&");
		test_sat("AB&!C!|");
		test_sat("AB|!C!&");
	}
}
