/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 11:01:09 by jayache           #+#    #+#             */
/*   Updated: 2021/12/01 11:14:00 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>

#pragma weak main

uint32_t	adder(uint32_t a, uint32_t b)
{
  uint32_t carry;

  while (b != 0)
  {
  	carry = a & b;
  	a = a ^ b;
  	b = carry << 1;
  }
  return (a);
}

void	test_adder(uint32_t a, uint32_t b)
{
	printf("%u + %u = %u = %u\n", a, b, a + b, adder(a, b));
}

int main(void)
{
	test_adder(0, 0);
	for (int i = 0; i < 255; ++i)
		test_adder(i, 1);
	for (int i = 0; i < 255; ++i)
		test_adder(255, i);
	test_adder(0xffffffff, 0xff);
}
