/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 13:24:27 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 11:03:11 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

#pragma weak main

int32_t	**powerset(int32_t *set)
{
	int32_t	**ret;
	int		temp_size;
	int		size_of_set;

	size_of_set = set_size(set);
	ret = (int32_t**)malloc(((1 << size_of_set) + 1) * sizeof(int32_t*));
	ret[1 << size_of_set] = 0;
	for (int64_t i = 0; i < (1 << size_of_set); ++i)
	{
		temp_size = 0;
		for (int x = 0; x < 64; ++x)
			temp_size += (i >> x) & 1;
		ret[i] = new_empty_set(temp_size);
		temp_size = 0;
		for (int x = 0; x < 64; ++x)
		{
			if ((i >> x) & 1)
			{
				ret[i][temp_size++] = set[x];
			}
		}
	}
	return (ret);
}

void	test_powerset(int32_t *set)
{
	int32_t **test;
	int		size;

	size = set_size(set);
	test = powerset(set);
	printf("P([");
	for (int i = 0; i < size; ++i)
		printf(" %d ", set[i]);
	printf("]) = \n");
	for (int i = 0; test[i]; ++i)
	{
		printf("[");
		size = set_size(test[i]);
		for (int x = 0; x < size; ++x)
			printf(" %d", test[i][x]);
		printf(" ]\n");
		free(test[i]);
	}
	free(test);
}

int main(void)
{
	int32_t	set[] = {4, 30, 100, 15, 7, 0};
	int32_t	setb[] = {1, 0};
	int32_t	setc[] = {0};
	int32_t	setd[] = {1, 2, 0, 0};
	int32_t	setf[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0};

	test_powerset(set);
	test_powerset(setb);
	test_powerset(setc);
	test_powerset(setd);
	test_powerset(setf);
}
