/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 13:13:13 by selver            #+#    #+#             */
/*   Updated: 2021/12/01 15:33:17 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TREE_H
# define TREE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

int			set_size(int32_t *set);

int32_t		*new_set(int size, ...);
int32_t		*new_empty_set(int size, ...);
int32_t		*new_set_copy(int32_t *set);

#endif
