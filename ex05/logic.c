/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logic.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 10:13:02 by selver            #+#    #+#             */
/*   Updated: 2021/11/29 10:30:04 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tree.h"
#include <stdbool.h>
#include <stdlib.h>

bool	replace_material_condition(t_node *tree)
{
	t_node	*c;
	bool	ret;

	ret = false;
	if (!tree)
		return (false);
	if (tree->value == '>')
	{
		ret = true;
		tree->value = '|';
		c = node('!');
		c->left = tree->left;
		tree->left = c;
	}
	ret |= replace_material_condition(tree->right);
	ret |= replace_material_condition(tree->left);
	return (ret);
}

bool	replace_equivalence(t_node *tree)
{
	t_node	*a;
	t_node	*b;
	bool	ret;

	ret = false;
	if (!tree)
		return (false);
	if (tree->value == '=')
	{
		tree->value = '&';
		a = tree->left;
		b = tree->right;
		tree->left = node('>');
		tree->left->left = a;
		tree->left->right = b;
		a = node_copy(a);
		b = node_copy(b);
		tree->right= node('>');
		tree->right->left = b;
		tree->right->right = a;
		ret = true;
	}
	else if (tree->value == '^')
	{
		tree->value = '!';
		a = node('=');
		a->left = tree->left;
		a->right = tree->right;
		tree->right = NULL;
		tree->left = a;
	}
	ret |= replace_equivalence(tree->left);
	ret |= replace_equivalence(tree->right);
	return (ret);
}

bool	remove_double_negation(t_node *tree)
{
	t_node	*n;
	bool	ret;

	ret = false;
	if (!tree)
		return (false);
	if (tree->value == '!')
	{
		if (tree->left->value == '!')
		{
			n = tree->left;
			tree->left = n->left->left;
			tree->right = n->left->right;
			tree->value = n->left->value;
			free(n->left);
			free(n);
			ret = true;
		}
	}
	ret |= remove_double_negation(tree->left);
	ret |= remove_double_negation(tree->right);
	return (ret);
}

bool	demorgan(t_node *tree)
{
	t_node	*c;
	bool	ret;

	ret = false;
	if (!tree)
		return (false);
	if (tree->value == '!')
	{
		if (tree->left->value == '|' || (tree->right && tree->right->value))
		{
			c = node('!');
			tree->right = c;
			tree->value = '&';
			tree->left->value = '!';
			tree->right->left = tree->left->right;
			tree->left->right = NULL;
			ret = true;
		}
		else if (tree->left->value == '&')
		{
			c = node('!');
			tree->right = c;
			tree->value = '|';
			tree->left->value = '!';
			tree->right->left = tree->left->right;
			tree->left->right = NULL;
			ret = true;
		}
	}
	ret |= demorgan(tree->right);
	ret |= demorgan(tree->left);
	return (ret);
}

void	generate_nnf(t_node *tree)
{
	bool changed;

	changed = true;
	while (changed)
	{
		changed = false;
		changed |= replace_equivalence(tree);
		changed |= replace_material_condition(tree);
		changed |= demorgan(tree);
		changed |= remove_double_negation(tree);
	}
}
