/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 13:13:13 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 10:38:29 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TREE_H
# define TREE_H

#include <stdbool.h>

typedef struct	s_node {
	char			value;
	struct s_node	*right;
	struct s_node	*left;
}				t_node;

t_node		*node(char value);
t_node		*node_copy(t_node *node);
t_node		*generate_ast(char *formula);

void		print_tree(t_node *node, int level);
void		generate_nnf(t_node *tree);
void		free_tree(t_node *tree);

int			tree_size(t_node *tree);

char		*formula_from_ast(t_node *tree);

bool		replace_material_condition(t_node *tree);
bool		replace_equivalence(t_node *tree);
bool		demorgan(t_node *tree);
bool		remove_double_negation(t_node *tree);

#endif
