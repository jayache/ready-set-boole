/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:02:56 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

#pragma weak main

char	*negation_normal_form(char *formula)
{
	t_node	*tree;
	char	*ret;

	tree = generate_ast(formula);
	generate_nnf(tree);
	ret = formula_from_ast(tree);
	free_tree(tree);
	return (ret);
}

void	test_nnf(char *formula)
{
	char	*form;

	form = negation_normal_form(formula);
	printf("%s -> %s\n", formula, form);
	free(form);
}

int main(int ac, char **av)
{
	if (ac != 1)
	{
		t_node *tree = generate_ast(av[1]);
		test_nnf(av[1]);
		generate_nnf(tree);
		print_tree(tree, 0);
		free_tree(tree);
	}
	else
	{
		test_nnf("AB&!");
		test_nnf("AB|!");
		test_nnf("AB>");
		test_nnf("AB^");
		test_nnf("AB=");
		test_nnf("AB|C&!");
	}
}
