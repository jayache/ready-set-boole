/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/30 10:36:35 by selver            #+#    #+#             */
/*   Updated: 2021/11/30 11:15:22 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

int		set_size(int32_t *set)
{
	int	size;

	for (size = 0; set[size]; ++size);
	return (size);
}

int32_t	*new_empty_set(int size)
{
	int32_t	*set;

	set = (int32_t*)malloc((size + 1) * sizeof(int32_t));
	for (int i = 0; i < size + 1; ++i)
		set[i] = 0;
	return (set);
}

int32_t	*new_set_copy(int32_t *set)
{
	int		size;
	int32_t	*ret;

	size = set_size(set);
	ret = new_empty_set(size);
	for (int i = 0; i < size; ++i)
		ret[i] = set[i];
	return (ret);
}

int32_t	*new_set(int size, ...)
{
	int32_t	*set;
	va_list ap;
	int i;

	set = (int32_t*)malloc((size + 1) * sizeof(int32_t));
	set[size] = 0;
	va_start(ap, size);
	for(i = 0; i < size; i++) {
		set[i] = va_arg(ap, int32_t);
	}
	va_end(ap);

	return (set);
}


int32_t	*set_negation(int32_t *a, int32_t **universe)
{
	int32_t	*ret;
	int		set_a_size;
	int		set_b_size;
	bool	found;
	int		position;

	position = 0;
	set_a_size = set_size(a);
	for (int i = 0; universe[i]; ++i)
	{
		set_b_size = set_size(universe[i]);
		for (int x = 0; x < set_b_size; ++x)
		{
			found = false;
			for (int z = 0; z < set_a_size; ++z)
			{
				found |= a[z] == universe[i][x];
			}
			if (!found)
				position++;
		}
	}
	ret = new_empty_set(position);
	position = 0;
	for (int i = 0; universe[i]; ++i)
	{
		set_b_size = set_size(universe[i]);
		for (int x = 0; x < set_b_size; ++x)
		{
			found = false;
			for (int z = 0; z < set_a_size; ++z)
			{
				found |= a[z] == universe[i][x];
			}
			for (int z = 0; z < position; ++z)
				found |= ret[z] == universe[i][x];
			if (!found)
			{
				ret[position++] = universe[i][x];
			}
		}
	}
	return (ret);
}

int32_t	*set_conjunction(int32_t *a, int32_t *b)
{
	int32_t	*ret;
	int		set_a_size;
	int		set_b_size;
	int		position;

	position = 0;
	set_a_size = set_size(a);
	set_b_size = set_size(b);
	for (int i = 0; i < set_a_size; ++i)
	{
		for (int x = 0; x < set_b_size; ++x)
		{
			if (a[i] == b[x])
				position++;
		}
	}
	ret = new_empty_set(position);
	position = 0;
	for (int i = 0; i < set_a_size; ++i)
	{
		for (int x = 0; x < set_b_size; ++x)
		{
			if (a[i] == b[x])
				ret[position++] = a[i];
		}
	}
	return (ret);
}

int32_t	*set_disjunction(int32_t *a, int32_t *b)
{
	int32_t	*ret;
	int		set_a_size;
	int		set_b_size;
	int		position;
	bool	found;

	position = 0;
	set_a_size = set_size(a);
	set_b_size = set_size(b);
	ret = new_empty_set(set_a_size + set_b_size);
	for (int i = 0; i < set_a_size; ++i)
	{
		ret[position++] = a[i];
	}
	for (int i = 0; i < set_b_size; ++i)
	{
		found = false;
		for (int x = 0; x < position; ++x)
			found |= b[i] == ret[x];
		if (!found)
			ret[position++] = b[i];
	}
	return (ret);
}

int32_t	*set_exclusive_disjunction(int32_t *a, int32_t *b)
{
	
	int32_t	*ret;
	int		set_a_size;
	int		set_b_size;
	int		position;
	bool	found;

	position = 0;
	set_a_size = set_size(a);
	set_b_size = set_size(b);
	ret = new_empty_set(set_a_size + set_b_size);
	for (int i = 0; i < set_a_size; ++i)
	{
		found = false;
		for (int x = 0; x < set_b_size; ++x)
			found |= b[x] == a[i];
		if (!found)
			ret[position++] = a[i];
	}
	for (int i = 0; i < set_b_size; ++i)
	{
		found = false;
		for (int x = 0; x < position; ++x)
			found |= b[i] == ret[x];
		for (int x = 0; x < set_a_size; ++x)
			found |= b[i] == a[x];
		if (!found)
			ret[position++] = b[i];
	}
	return (ret);
}
