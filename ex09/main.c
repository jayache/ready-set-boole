/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:03:15 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

#pragma weak main

int32_t	*eval_tree(t_node *node, int32_t **universe)
{
	char	val;
	t_node	*left;
	t_node	*right;
	int32_t	*a;
	int32_t	*b;
	int32_t	*ret;

	left = node->left;
	right = node->right;
	val = node->value;
	if (val >= 'A' && val <= 'Z')
		return (new_set_copy(universe[val - 'A']));
	else if (val == '!')
	{
		a = eval_tree(left, universe);
		ret = set_negation(a, universe);
		free(a);
		return (ret);
	}
	else if (val == '&')
	{
		a = eval_tree(left, universe);
		b = eval_tree(right, universe);
		ret = set_conjunction(a, b);
		free(a);
		free(b);
		return (ret);
	}
	else if (val == '|')
	{
		a = eval_tree(left, universe);
		b = eval_tree(right, universe);
		ret = set_disjunction(a, b);
		free(a);
		free(b);
		return (ret);
	}
	 else if (val == '^')
	{
		a = eval_tree(left, universe);
		b = eval_tree(right, universe);
		ret = set_exclusive_disjunction(a, b);
		free(a);
		free(b);
		return (ret);
	}
	return (false);
}

int32_t	*eval_sets(char *formula, int32_t **universe)
{
	int32_t *result;

	t_node *tree = generate_ast(formula);
	generate_nnf(tree);
	result = eval_tree(tree, universe);
	free(tree);
	return (result);
}

void test_set_eval(char *formula, int32_t **universe)
{
	int		size;
	t_node	*tree;
	int32_t	*result;

	tree = generate_ast(formula);
	generate_nnf(tree);
	result = eval_tree(tree, universe);
	size = set_size(result);
	printf("%s -> [", formula);
	for (int x = 0; x < size; ++x)
	{
		printf(" %d ", result[x]);
	}
	free(result);
	free_tree(tree);
	printf("]\n");
}

int main(int ac, char **av)
{
	int32_t	a[] = {5, 0};
	int32_t	b[] = {5, 6, 0};
	int32_t	c[] = {8, 10, 9, 0};
	int32_t	d[] = {7, 5, 1, 9, 0};
	int32_t	*sets[] = {a, b, c, d, 0};
	int		size;

	printf("[\n");
	for (int i = 0; sets[i]; ++i)
	{
		printf("\t[");
		size = set_size(sets[i]);
		for (int x = 0; x < size; ++x)
		{
			printf(" %d ", sets[i][x]);
		}
		printf("]\n");
	}
	printf("]\n");
	if (ac != 1)
	{
		test_set_eval(av[1], sets);
	}
	else
	{
		test_set_eval("AB&", sets);
		test_set_eval("AB|", sets);
		test_set_eval("AB^", sets);
		test_set_eval("AB=", sets);
		test_set_eval("AB>", sets);
	}
}
