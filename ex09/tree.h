/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 13:13:13 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 10:50:54 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TREE_H
# define TREE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

typedef struct	s_node {
	char			value;
	struct s_node	*right;
	struct s_node	*left;
}				t_node;

t_node		*node(char value);
t_node		*node_copy(t_node *node);
t_node		*generate_ast(char *formula);

void		print_tree(t_node *node, int level);
void		generate_nnf(t_node *tree);
void		generate_cnf(t_node *tree);
void		free_tree(t_node *tree);

int			tree_size(t_node *tree);
int			set_size(int32_t *set);

char		*formula_from_ast(t_node *tree);

bool		replace_material_condition(t_node *tree);
bool		replace_equivalence(t_node *tree);
bool		demorgan(t_node *tree);
bool		remove_double_negation(t_node *tree);

int32_t		*new_set(int size, ...);
int32_t		*new_empty_set(int size, ...);
int32_t		*new_set_copy(int32_t *set);

int32_t		*set_negation(int32_t *a, int32_t **universe);
int32_t		*set_conjunction(int32_t *a, int32_t *b);
int32_t		*set_disjunction(int32_t *a, int32_t *b);
int32_t		*set_exclusive_disjunction(int32_t *a, int32_t *b);

#endif
