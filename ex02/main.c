/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 14:04:34 by jayache           #+#    #+#             */
/*   Updated: 2021/12/02 11:02:14 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>

#pragma weak main

uint32_t gray_code(uint32_t n)
{
	return (n ^ (n >> 1));
}

int main(void)
{
	printf("%d\n", gray_code(0));
	printf("%d\n", gray_code(1));
	printf("%d\n", gray_code(2));
	printf("%d\n", gray_code(3));
	printf("%d\n", gray_code(4));
	printf("%d\n", gray_code(5));
	printf("%d\n", gray_code(6));
	printf("%d\n", gray_code(7));
	printf("%d\n", gray_code(8));
	printf("%d\n", gray_code(9));
}
