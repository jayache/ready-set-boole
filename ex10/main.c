/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 11:07:13 by selver            #+#    #+#             */
/*   Updated: 2021/12/02 11:03:20 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>

#pragma weak main

double map(uint16_t x, uint16_t y)
{
	double ret;

	ret = (((uint32_t)x << 16) + y) / (double)0xffffffff;
	return (ret);
}

void	test_map(uint16_t x, uint16_t y)
{
	printf("(%u, %u) -> %.15lf\n", x, y, map(x, y));
}

int main()
{
	for (int i = 1; i < 0xffff; i *= 2)
		test_map(i - 1, 5);
	for (int i = 1; i < 0xffff; i *= 2)
		test_map(0xffff - 1, i - 1);
}
